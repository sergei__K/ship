import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String args[]) throws IOException {
        Ship ship = new Ship();
        ship.setLocation(Initialization());
        int countOfShots = 1;
        String resultShot;
        System.out.println("Игра Морской бой");
        System.out.println("Введите координаты выстрела");

        while ((resultShot = ship.checkShip(Shot())) != "Потоплен") {
            countOfShots++;
            System.out.println(resultShot + "  Введите координаты выстрела еще раз");
        }

        System.out.println("Вы потопили корабль \n Попыток использовано - " + countOfShots);
    }


    private static ArrayList Initialization() {
        ArrayList location = new ArrayList();
        int firstNum = (int) (Math.random() * 5);
        for (int i = 0; i < 3; i++) {
            location.add(Integer.toString(firstNum + i));
        }
        return location;
    }

    private static   String Shot() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String shot;
        do{
            shot = bufferedReader.readLine();
        }while (CheckShot(shot) != true);
        return shot;
    }



    private static boolean CheckShot(String shot){
        Pattern pattern = Pattern.compile("[0-6]");
        Matcher matcher = pattern.matcher(shot);
        return matcher.matches();
    }


}
