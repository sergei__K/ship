import java.util.ArrayList;

public class Ship {
    private ArrayList location;

    public void setLocation(ArrayList location) {
        this.location = location;
    }

    public String checkShip(String shot) {
        String result = "Мимо";
        int index;
        if ((index = location.indexOf(shot)) > (-1)) {
            location.remove(index);
            result = "Попал";
        }
        if (location.isEmpty()) {
            result = "Потоплен";
        }
        return result;

    }

    @Override
    public String toString() {
        return "Ship{" +
                "location=" + location +
                '}';
    }
}
